/**
 * @file
 * Redirects to Webpay from return.
 */

(function ($) {
  $(document).ready(function () {
    $('#form-redirect-webpay').submit();
  });
})(jQuery);
