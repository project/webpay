<?php

namespace Drupal\webpay;

use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Link;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines a class to build a listing of Webpay transaction entities.
 *
 * @ingroup webpay
 */
class WebpayTransactionListBuilder extends EntityListBuilder {

  /**
   * The date formatter service.
   *
   * @var \Drupal\Core\Datetime\DateFormatterInterface
   */
  protected $dateFormatter;

  /**
   * WebpayTransactionListBuilder constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity type definition.
   * @param \Drupal\Core\Entity\EntityStorageInterface $storage
   *   The entity storage class.
   * @param \Drupal\Core\Datetime\DateFormatterInterface $date_formatter
   *   The date formatter service.
   */
  public function __construct(EntityTypeInterface $entity_type, EntityStorageInterface $storage, DateFormatterInterface $date_formatter) {
    parent::__construct($entity_type, $storage);

    $this->dateFormatter = $date_formatter;
  }

  /**
   * {@inheritdoc}
   */
  public static function createInstance(ContainerInterface $container, EntityTypeInterface $entity_type) {
    return new static(
      $entity_type,
      $container->get('entity_type.manager')->getStorage($entity_type->id()),
      $container->get('date.formatter')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['id'] = $this->t('ID');
    $header['config_id'] = $this->t('Commerce');
    $header['order_number'] = $this->t('Order number');
    $header['commerce_system_id'] = $this->t('Commerce system');
    $header['amount'] = $this->t('Amount');
    $header['transaction_date'] = $this->t('Transaction Date');
    $header['response_code'] = $this->t('Response code');

    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /** @var \Drupal\webpay\Entity\WebpayTransactionInterface $entity */
    $row['id'] = $entity->id();

    if (!$entity->get('config_id')->entity) {
      $row['config_id'] = $this->t('No configuration');
    }
    else {
      $config = $entity->get('config_id');
      $row['config_id'] = Link::createFromRoute(
        $config->entity->label(),
        'entity.webpay_config.canonical',
        ['webpay_config' => $config->entity->id()]
      );
    }
    $row['order_number'] = $entity->get('order_number')->value;
    $row['commerce_system_id'] = $entity->get('commerce_system_id')->value;
    $row['amount'] = $entity->get('amount')->value;
    $row['transaction_date'] = $this->dateFormatter->format($entity->get('transaction_date')->value);
    $row['response_code'] = $entity->get('response_code')->value;

    return $row + parent::buildRow($entity);
  }

  /**
   * {@inheritdoc}
   */
  protected function getEntityIds() {
    $query = $this->getStorage()->getQuery()
      ->sort($this->entityType->getKey('id'), 'DESC');

    if ($this->limit) {
      $query->pager($this->limit);
    }
    return $query->execute();
  }

  /**
   * {@inheritdoc}
   */
  protected function getDefaultOperations(EntityInterface $entity) {
    $operations = [];
    $operations['view'] = [
      'title' => $this->t('View'),
      'url' => $entity->toUrl(),
    ];

    return $operations;
  }

}
