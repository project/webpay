<?php

namespace Drupal\webpay\Controller;

use Drupal\Core\Render\AttachmentsResponseProcessorInterface;
use Drupal\Core\Render\RendererInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\webpay\WebpayTransactionStorageInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Drupal\webpay\Entity\WebpayTransaction;
use Drupal\Core\Render\BareHtmlPageRenderer;
use Drupal\Core\Controller\ControllerBase;
use Drupal\webpay\Entity\WebpayConfig;
use Drupal\webpay\Entity\WebpayConfigInterface;
use Symfony\Component\HttpFoundation\Request;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Session\AccountInterface;
use Drupal\webpay\WebpayNormalService;

/**
 * Class WebpayController.
 */
class WebpayController extends ControllerBase {

  /**
   * The webpay transaction storage class.
   *
   * @var \Drupal\webpay\WebpayTransactionStorageInterface
   */
  protected $webpayTransactionStorage;

  /**
   * The html response attachedments processor.
   *
   * @var \Drupal\Core\Render\AttachmentsResponseProcessorInterface
   */
  protected $htmlResponseAttachmentsProcessor;

  /**
   * The renderer.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected $renderer;

  /**
   * Constructs the WebpayController object.
   *
   * @param \Drupal\webpay\WebpayTransactionStorageInterface $webpay_transaction_storage
   *   The webpay transaction storage class.
   * @param \Drupal\Core\Render\AttachmentsResponseProcessorInterface $html_response_attachments_processor
   *   The html response attachedments processor.
   * @param \Drupal\Core\Render\RendererInterface $renderer
   *   The renderer.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger_factory
   *   The logger factory.
   */
  public function __construct(WebpayTransactionStorageInterface $webpay_transaction_storage, AttachmentsResponseProcessorInterface $html_response_attachments_processor, RendererInterface $renderer, LoggerChannelFactoryInterface $logger_factory) {
    $this->webpayTransactionStorage = $webpay_transaction_storage;
    $this->htmlResponseAttachmentsProcessor = $html_response_attachments_processor;
    $this->renderer = $renderer;
    $this->loggerFactory = $logger_factory;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager')->getStorage('webpay_transaction'),
      $container->get('html_response.attachments_processor'),
      $container->get('renderer'),
      $container->get('logger.factory')
    );
  }

  /**
   * Return page.
   *
   * @param string $commerce_system_id
   *   The commerce system plugin id.
   * @param \Drupal\webpay\Entity\WebpayConfigInterface $webpay_config
   *   The webpay config.
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The request object.
   *
   * @return array|\Drupal\Core\Render\AttachmentsInterface|\Drupal\Core\Render\HtmlResponse|\Symfony\Component\HttpFoundation\RedirectResponse
   *   The response depending on many conditions.
   */
  public function return($commerce_system_id, WebpayConfigInterface $webpay_config, Request $request) {
    $token = $request->get('token_ws');

    if (!$token) {
      throw new AccessDeniedHttpException();
    }

    try {
      $webpayService = new WebpayNormalService($webpay_config, $commerce_system_id);
    }
    catch (\Exception $err) {
      return $this->returnErrorMessage();
    }
    $response = $webpayService->getTransactionResult($token);

    if (!$response) {
      return $this->returnErrorMessage();
    }

    /** @var \Drupal\webpay\Entity\WebpayTransactionInterface $transaction */
    $transaction = WebpayTransaction::create([
      'config_id' => $webpay_config->id(),
      'commerce_code' => $webpay_config->get('commerce_code'),
      'commerce_system_id' => $commerce_system_id,
      'token' => $token,
      'order_number' => $response->buyOrder,
      'session_id' => $response->sessionId,
      'transaction_date' => strtotime($response->transactionDate),
      'vci' => $response->VCI,
      'card_number' => $response->cardDetail->cardNumber,
      'authorization_code' => $response->detailOutput->authorizationCode,
      'payment_type_code' => $response->detailOutput->paymentTypeCode,
      'response_code' => $response->detailOutput->responseCode,
      'amount' => $response->detailOutput->amount,
      'shares_number' => $response->detailOutput->sharesNumber,
    ]);

    try {
      $transaction->save();
    }
    catch (\Exception $err) {
      $log_message = 'Failed to save the transaction in the database: @err';
      $log_variable = ['@err' => $err->getMessage()];

      $this->loggerFactory->get('webpay')->error($log_message, $log_variable);

      return $this->returnErrorMessage();
    }

    if (($transaction->get('vci')->value == "TSY" || $transaction->get('vci')->value == "") && $transaction->get('response_code')->value === 0) {

      $webpayService->invokeTransactionAccepted($transaction);

      $bareHtmlPageRenderer = new BareHtmlPageRenderer($this->renderer, $this->htmlResponseAttachmentsProcessor);
      $response = $bareHtmlPageRenderer->renderBarePage([], $this->t('Webpay Return'), 'webpay_return', [
        '#token' => $token,
        '#url' => $response->urlRedirection,
        '#attached' => [
          'library' => [
            'webpay/return',
          ],
        ],
      ]);

      return $response;
    }

    return $webpayService->invokeTransactionRejected($transaction);
  }

  /**
   * Failure page.
   *
   * @param string $token
   *   The token from webpay.
   *
   * @return array
   *   The build.
   */
  public function failure($token) {
    if (!$transaction = $this->webpayTransactionStorage->byToken($token)) {
      throw new AccessDeniedHttpException();
    }

    return [
      '#theme' => 'webpay_failure',
      '#order_id' => $transaction->get('order_number')->value,
    ];
  }

  /**
   * Logs page of a webpay config.
   *
   * @param \Drupal\webpay\Entity\WebpayConfigInterface $webpay_config
   *   The webpay config entity.
   *
   * @return array
   *   The build.
   */
  public function logs(WebpayConfigInterface $webpay_config) {
    $build = [];

    $logs = $webpay_config->getLogs();

    if (!empty($logs)) {
      foreach ($logs as $name => $log) {
        $build[$name] = [
          '#type' => 'details',
          '#title' => $name,
        ];
        $build[$name]['logs'] = [
          '#type' => 'inline_template',
          '#template' => '<textarea style="width: 100%; height: 400px; resize: none;" readonly>{{ log }}</textarea>',
          '#context' => ['log' => $log],
        ];
      }
    }
    else {
      $build['empty'] = [
        '#markup' => $this->t('No logs'),
      ];
    }

    return $build;
  }

  /**
   * Helper method to show a simple message to the page.
   *
   * @return array
   *   The error message.
   */
  protected function returnErrorMessage() {
    $message = $this->t('If the error persists, please contact the site administrator.');
    $title = $this->t('Some error detected');

    return [
      '#title' => $title,
      '#type' => 'inline_template',
      '#template' => $message,
    ];
  }

  /**
   * Check access to create the certification configuration.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The account entity.
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   *   The Access result class.
   */
  public function addCertificationWebpayAccess(AccountInterface $account) {
    $cache_tags = ['config:webpay_config_list'];
    $config = WebpayConfig::load('certification');
    if ($config) {
      return AccessResult::forbidden()->addCacheTags($cache_tags);
    }

    return AccessResult::allowedIfHasPermission($account, 'webpay administer')->addCacheTags($cache_tags);
  }

}
