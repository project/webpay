<?php

namespace Drupal\webpay\Plugin;

use Drupal\Component\Plugin\PluginInspectionInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\webpay\Entity\WebpayConfigInterface;
use Drupal\webpay\Entity\WebpayTransactionInterface;

/**
 * Defines an interface for Webpay commerce system plugins.
 */
interface WebpayCommerceSystemInterface extends PluginInspectionInterface, ContainerFactoryPluginInterface {

  /**
   * This method is invoked when webpay accept the transaction.
   *
   * Here you can finish a local transaction of the commerce system.
   *
   * @param \Drupal\webpay\Entity\WebpayConfigInterface $webpay_config
   *   The configuration of the commerce code.
   * @param \Drupal\webpay\Entity\WebpayTransactionInterface $transaction
   *   The WebpayTransaction object.
   */
  public function transactionAccepted(WebpayConfigInterface $webpay_config, WebpayTransactionInterface $transaction);

  /**
   * This method is invoked when webpay reject the transaction.
   *
   * Here you can cancel a local transaction of the commerce system.
   *
   * @param \Drupal\webpay\Entity\WebpayConfigInterface $webpay_config
   *   The configuration of the commerce code.
   * @param \Drupal\webpay\Entity\WebpayTransactionInterface $transaction
   *   The WebpayTransaction object.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   *   A Redirect Response object.
   */
  public function transactionRejected(WebpayConfigInterface $webpay_config, WebpayTransactionInterface $transaction);

}
