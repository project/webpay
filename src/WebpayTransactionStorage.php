<?php

namespace Drupal\webpay;

use Drupal\Core\Entity\Sql\SqlContentEntityStorage;

/**
 * Class WebpayTransactionStorage.
 *
 * @package Drupal\webpay
 */
class WebpayTransactionStorage extends SqlContentEntityStorage implements WebpayTransactionStorageInterface {

  /**
   * {@inheritdoc}
   */
  public function byToken($token) {
    $transactions = $this->loadByProperties(['token' => $token]);

    if (!empty($transactions)) {
      return end($transactions);
    }

    return FALSE;
  }

}
