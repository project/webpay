<?php

namespace Drupal\webpay;

use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Url;
use Freshwork\Transbank\CertificationBag;
use Freshwork\Transbank\TransbankServiceFactory;
use Drupal\webpay\Entity\WebpayConfigInterface;
use Drupal\webpay\Entity\WebpayTransactionInterface;

/**
 * The services Webpay Normal.
 */
class WebpayNormalService {

  use StringTranslationTrait;

  /**
   * Records errors messages.
   *
   * @var array
   */
  protected $errors = [];

  /**
   * The webpay config of a commerce code.
   *
   * @var \Drupal\webpay\Entity\WebpayConfigInterface
   */
  protected $webpayConfig;

  /**
   * The Transbank service.
   *
   * @var \Freshwork\Transbank\WebpayWebService
   */
  protected $service;

  /**
   * The commerce system id.
   *
   * @var string
   */
  protected $commerceSystemPluginId;

  /**
   * The commerce system plugin instance.
   *
   * @var \Drupal\webpay\Plugin\WebpayCommerceSystemInterface
   */
  protected $commerceSystemPlugin;

  /**
   * Constructs the WebpayNormalService object.
   *
   * @param \Drupal\webpay\Entity\WebpayConfigInterface $webpay_config
   *   The webpay configuration instance.
   * @param string $commerceSystemPluginId
   *   The commerce system plugin id.
   */
  public function __construct(WebpayConfigInterface $webpay_config, $commerceSystemPluginId) {
    $this->webpayConfig = $webpay_config;

    $this->commerceSystemPluginId = $commerceSystemPluginId;
    $this->commerceSystemPlugin = \Drupal::service('plugin.manager.webpay_commerce_system')
      ->createInstance($commerceSystemPluginId);

    if ($webpay_config->get('log')) {
      $webpay_config->activeLog();
    }

    $bag = new CertificationBag(
      $webpay_config->get('private_key'),
      $webpay_config->get('client_certificate'),
      $webpay_config->get('server_certificate'),
      $webpay_config->get('environment')
    );

    $this->service = TransbankServiceFactory::normal($bag);
  }

  /**
   * Execute the initTransaction Services.
   *
   * @param int $buyOrder
   *   The order number.
   * @param float $amount
   *   The amount.
   * @param \Drupal\Core\Url $finalUrl
   *   The final url.
   * @param mixed $session_id
   *   The session id.
   *
   * @return mixed
   *   The response object from services of the transbank.
   */
  public function initTransaction($buyOrder, $amount, Url $finalUrl, $session_id = NULL) {
    $service = $this->service;

    $service->addTransactionDetail($amount, $buyOrder);

    $returnUrl = new Url('webpay.webpay_controller_return', [
      'commerce_system_id' => $this->commerceSystemPluginId,
      'webpay_config' => $this->webpayConfig->id(),
    ], ['absolute' => TRUE]);

    return $service->initTransaction($returnUrl->toString(), $finalUrl->toString(), $session_id);
  }

  /**
   * Execute the getTransactionResult Service.
   *
   * @param string $token
   *   The token response by transbank.
   *
   * @return mixed
   *   The response of transbank or FALSE.
   */
  public function getTransactionResult($token) {
    try {
      $response = $this->service->getTransactionResult($token);
      if ($response) {
        $isValid = $this->service->acknowledgeTransaction($token);
        if ($isValid) {
          return $response;
        }
        else {
          $this->addError('acknowledgeTransaction', $this->t('The transaction is not valid. Check the keys.'));
        }
      }
    }
    catch (\Exception $e) {
      $this->addError('getTransactionResult', $this->t('The transaction is not valid. Check the keys.'));
    }

    return FALSE;
  }

  /**
   * Invoke the transaction accepted.
   *
   * @param \Drupal\webpay\Entity\WebpayTransactionInterface $transaction
   *   The transaction entity.
   */
  public function invokeTransactionAccepted(WebpayTransactionInterface $transaction) {
    $this->commerceSystemPlugin->transactionAccepted($this->webpayConfig, $transaction);
  }

  /**
   * Invoke the transaction rejected.
   *
   * @param \Drupal\webpay\Entity\WebpayTransactionInterface $transaction
   *   The transaction entity.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   *   A redirect response object.
   */
  public function invokeTransactionRejected(WebpayTransactionInterface $transaction) {
    return $this->commerceSystemPlugin->transactionRejected($this->webpayConfig, $transaction);
  }

  /**
   * Get errors.
   *
   * @param string|null $type
   *   A type of error.
   *
   * @return array|mixed
   *   The list of errors.
   */
  public function getErrors($type = NULL) {
    if ($type && isset($this->errors[$type])) {
      $errors = $this->errors[$type];
    }
    else {
      $errors = $this->errors;
    }
    $this->clearErrors($type);

    return $errors;
  }

  /**
   * Add an error message from any type.
   *
   * @param string $type
   *   A type of error.
   * @param string $message
   *   A message of error.
   */
  protected function addError($type, $message) {
    $this->errors[$type][] = $message;
  }

  /**
   * A helper function.
   *
   * Cleans the errors array of some type or all types.
   *
   * @param mixed $type
   *   A type of error.
   *
   * @return \Drupal\webpay\WebpayNormalService
   *   The WebpayNormalService instance.
   */
  protected function clearErrors($type = NULL) {
    if ($type && isset($this->errors[$type])) {
      unset($this->errors[$type]);
    }
    else {
      $this->errors = [];
    }

    return $this;
  }

}
