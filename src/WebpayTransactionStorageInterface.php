<?php

namespace Drupal\webpay;

/**
 * Interface WebpayTransactionStorageInterface.
 *
 * @package Drupal\webpay
 */
interface WebpayTransactionStorageInterface {

  /**
   * Given a token, returns the transaction made.
   *
   * @param string $token
   *   The token returned by webpay.
   *
   * @return \Drupal\webpay\Entity\WebpayTransactionInterface|bool
   *   If a Transaction exists for the token then returns the WebpayTransaction
   *   with the data. Otherwise it returns boolean false.
   */
  public function byToken($token);

}
