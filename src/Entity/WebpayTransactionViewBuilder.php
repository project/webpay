<?php

namespace Drupal\webpay\Entity;

use Drupal\Core\Entity\EntityViewBuilder;

/**
 * View builder handler for webpay transaction.
 */
class WebpayTransactionViewBuilder extends EntityViewBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildComponents(array &$build, array $entities, array $displays, $view_mode) {
    /** @var \Drupal\webpay\Entity\WebpayTransactionInterface[] $entities */
    if (empty($entities)) {
      return;
    }

    parent::buildComponents($build, $entities, $displays, $view_mode);

    foreach ($entities as $id => $entity) {
      if ($displays[$entity->bundle()]->getComponent('voucher')) {
        $build[$id]['voucher'] = [
          '#theme' => 'webpay_voucher',
          '#transaction' => $entity,
        ];
      }
    }
  }

}
