<?php

namespace Drupal\webpay\Entity;

use Drupal\views\EntityViewsData;

/**
 * Provides Views data for Webpay transaction entities.
 */
class WebpayTransactionViewsData extends EntityViewsData {

  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();

    $data['webpay_transaction']['created_fulldate'] = [
      'title' => $this->t('Created date'),
      'help' => $this->t('Date in the form of CCYYMMDD.'),
      'argument' => [
        'field' => 'created',
        'id' => 'date_fulldate',
      ],
    ];

    $data['webpay_transaction']['created_year_month'] = [
      'title' => $this->t('Created year + month'),
      'help' => $this->t('Date in the form of YYYYMM.'),
      'argument' => [
        'field' => 'created',
        'id' => 'date_year_month',
      ],
    ];

    $data['webpay_transaction']['created_year'] = [
      'title' => $this->t('Created year'),
      'help' => $this->t('Date in the form of YYYY.'),
      'argument' => [
        'field' => 'created',
        'id' => 'date_year',
      ],
    ];

    $data['webpay_transaction']['created_month'] = [
      'title' => $this->t('Created month'),
      'help' => $this->t('Date in the form of MM (01 - 12).'),
      'argument' => [
        'field' => 'created',
        'id' => 'date_month',
      ],
    ];

    $data['webpay_transaction']['created_day'] = [
      'title' => $this->t('Created day'),
      'help' => $this->t('Date in the form of DD (01 - 31).'),
      'argument' => [
        'field' => 'created',
        'id' => 'date_day',
      ],
    ];

    $data['webpay_transaction']['created_week'] = [
      'title' => $this->t('Created week'),
      'help' => $this->t('Date in the form of WW (01 - 53).'),
      'argument' => [
        'field' => 'created',
        'id' => 'date_week',
      ],
    ];

    $data['webpay_transaction']['transaction_date_fulldate'] = [
      'title' => $this->t('Transaction date'),
      'help' => $this->t('Date in the form of CCYYMMDD.'),
      'argument' => [
        'field' => 'transaction_date',
        'id' => 'date_fulldate',
      ],
    ];

    $data['webpay_transaction']['transaction_date_year_month'] = [
      'title' => $this->t('Transaction date year + month'),
      'help' => $this->t('Date in the form of YYYYMM.'),
      'argument' => [
        'field' => 'transaction_date',
        'id' => 'date_year_month',
      ],
    ];

    $data['webpay_transaction']['transaction_date_year'] = [
      'title' => $this->t('Transaction year'),
      'help' => $this->t('Date in the form of YYYY.'),
      'argument' => [
        'field' => 'transaction_date',
        'id' => 'date_year',
      ],
    ];

    $data['webpay_transaction']['transaction_date_month'] = [
      'title' => $this->t('Transaction month'),
      'help' => $this->t('Date in the form of MM (01 - 12).'),
      'argument' => [
        'field' => 'transaction_date',
        'id' => 'date_month',
      ],
    ];

    $data['webpay_transaction']['transaction_date_day'] = [
      'title' => $this->t('Transaction day'),
      'help' => $this->t('Date in the form of DD (01 - 31).'),
      'argument' => [
        'field' => 'transaction_date',
        'id' => 'date_day',
      ],
    ];

    $data['webpay_transaction']['transaction_date_week'] = [
      'title' => $this->t('Transaction week'),
      'help' => $this->t('Date in the form of WW (01 - 53).'),
      'argument' => [
        'field' => 'transaction_date',
        'id' => 'date_week',
      ],
    ];

    return $data;
  }

}
