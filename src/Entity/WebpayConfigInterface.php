<?php

namespace Drupal\webpay\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface for defining Webpay config entities.
 */
interface WebpayConfigInterface extends ConfigEntityInterface {

  /**
   * Get the Environment id.
   *
   * @return string
   *   The environment id.
   */
  public function getEnvironmentId();

  /**
   * Get the Environment name.
   *
   * @return string|null
   *   The environment name.
   */
  public function getEnvironment();

  /**
   * Active the log system.
   */
  public function activeLog();

  /**
   * Get all logs.
   *
   * @return array
   *   The logs.
   */
  public function getLogs();

  /**
   * Helper function to get path files for webpay config.
   *
   * @param string $suffix
   *   The name of the folder.
   * @param bool $create
   *   Create the folder if does not exists.
   *
   * @return string|bool
   *   The path/uri or false if exists some error.
   */
  public static function getPathFiles($suffix, $create = FALSE);

}
