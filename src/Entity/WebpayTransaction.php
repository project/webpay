<?php

namespace Drupal\webpay\Entity;

use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Defines the Webpay transaction entity.
 *
 * @ingroup webpay
 *
 * @ContentEntityType(
 *   id = "webpay_transaction",
 *   label = @Translation("Webpay transaction"),
 *   label_collection = @Translation("Webpay Transactions"),
 *   handlers = {
 *     "storage" = "Drupal\webpay\WebpayTransactionStorage",
 *     "view_builder" = "Drupal\webpay\Entity\WebpayTransactionViewBuilder",
 *     "list_builder" = "Drupal\webpay\WebpayTransactionListBuilder",
 *     "views_data" = "Drupal\webpay\Entity\WebpayTransactionViewsData",
 *     "access" = "Drupal\webpay\WebpayTransactionAccessControlHandler",
 *   },
 *   base_table = "webpay_transaction",
 *   admin_permission = "administer webpay transaction entities",
 *   entity_keys = {
 *     "id" = "id",
 *     "uuid" = "uuid",
 *   },
 *   links = {
 *     "canonical" = "/admin/config/webpay/webpay_transaction/{webpay_transaction}",
 *     "collection" = "/admin/config/webpay/webpay_transaction",
 *   },
 *   field_ui_base_route = "entity.webpay_transaction.collection",
 * )
 */
class WebpayTransaction extends ContentEntityBase implements WebpayTransactionInterface {

  use StringTranslationTrait;

  /**
   * {@inheritdoc}
   */
  public function label() {
    return $this->t('Transaction: %id', ['%id' => $this->id()]);
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp) {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getPaymentType() {
    $type_code = $this->get('payment_type_code')->value;
    $payment_type = self::getDefinitionPaymentType($type_code);

    return $payment_type['payment_type'];
  }

  /**
   * {@inheritdoc}
   */
  public function getQuotaType() {
    $type_code = $this->get('payment_type_code')->value;
    $payment_type = self::getDefinitionPaymentType($type_code);

    return $payment_type['quota_type'];
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['config_id'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Configuration'))
      ->setSetting('target_type', 'webpay_config')
      ->setRequired(TRUE)
      ->setReadOnly(TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['commerce_system_id'] = self::createFieldString(t('Commerce system ID'), 255, t('The commerce system used.'));
    $fields['token'] = self::createFieldString(t('Token'), 64, t('The token returned by webpay.'), FALSE);
    $fields['order_number'] = self::createFieldString(t('Order number'), 26);
    $fields['session_id'] = self::createFieldString(t('Session ID'), 61, NULL, FALSE);
    $fields['vci'] = self::createFieldString(t('VCI'), 3);
    $fields['card_number'] = self::createFieldString(t('Card Number'), 4);
    $fields['authorization_code'] = self::createFieldString(t('Authorization code'), 6);
    $fields['payment_type_code'] = self::createFieldString(t('Payment type code'), 2);

    $fields['transaction_date'] = BaseFieldDefinition::create('timestamp')
      ->setLabel(t('Transaction date'))
      ->setDisplayConfigurable('view', TRUE);
    $fields['response_code'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Response code'))
      ->setDisplayConfigurable('view', TRUE);
    $fields['amount'] = BaseFieldDefinition::create('float')
      ->setLabel(t('Amount'))
      ->setDisplayConfigurable('view', TRUE);
    $fields['shares_number'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Shares number'))
      ->setDisplayConfigurable('view', TRUE);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time that the entity was created.'));

    return $fields;
  }

  /**
   * Helper method to create a simple string field.
   *
   * @param \Drupal\Core\StringTranslation\TranslatableMarkup|string|null $label
   *   The label of the field.
   * @param int $max_length
   *   The max length of the field.
   * @param \Drupal\Core\StringTranslation\TranslatableMarkup|string|null $description
   *   The description of the field.
   * @param bool $display_configurable
   *   Set whether the field will be configurable in display view.
   *
   * @return \Drupal\Core\Field\BaseFieldDefinition|\Drupal\Core\TypedData\DataDefinition
   *   The field definition.
   */
  protected static function createFieldString($label, $max_length = 255, $description = NULL, $display_configurable = TRUE) {
    return BaseFieldDefinition::create('string')
      ->setLabel($label)
      ->setRequired(TRUE)
      ->setSetting('max_length', $max_length)
      ->setDescription($description)
      ->setDisplayConfigurable('view', $display_configurable);
  }

  /**
   * Given a payment code returns the string that represents that code.
   *
   * @param string $payment_type_code
   *   payment type code, used by Webpay.
   * @param array $options
   *   An associative array of additional options, with the following elements:
   *   - 'langcode' (defaults to the current language): The language code to
   *     translate to a language other than what is used to display the page.
   *   - 'context' (defaults to the empty context): The context the source
   *     string belongs to.
   *
   * @return array
   *   Returns the equivalent text string.
   */
  public static function getDefinitionPaymentType($payment_type_code, array $options = []) {
    $credit = t("Credit", [], $options);
    switch ($payment_type_code) {
      case 'VN':
        return [
          'payment_type' => $credit,
          'quota_type' => t("Without quotas", [], $options),
        ];

      case 'VC':
        return [
          'payment_type' => $credit,
          'quota_type' => t("Quota normal", [], $options),
        ];

      case 'SI':
      case 'S2':
      case 'NC':
        return [
          'payment_type' => $credit,
          'quota_type' => t("No Interest", [], $options),
        ];

      case 'CI':
        return [
          'payment_type' => $credit,
          'quota_type' => t("Commerce Quotas", [], $options),
        ];

      case 'VD':
        return [
          'payment_type' => t("RedCompra", [], $options),
          'quota_type' => t("Debit", [], $options),
        ];
    }

    return [
      'payment_type' => t('Undefined'),
      'quota_type' => t('Undefined'),
    ];
  }

}
