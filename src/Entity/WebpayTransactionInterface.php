<?php

namespace Drupal\webpay\Entity;

use Drupal\Core\Entity\ContentEntityInterface;

/**
 * Provides an interface for defining Webpay transaction entities.
 *
 * @ingroup webpay
 */
interface WebpayTransactionInterface extends ContentEntityInterface {

  /**
   * Gets the Webpay transaction creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Webpay transaction.
   */
  public function getCreatedTime();

  /**
   * Sets the Webpay transaction creation timestamp.
   *
   * @param int $timestamp
   *   The Webpay transaction creation timestamp.
   *
   * @return \Drupal\webpay\Entity\WebpayTransactionInterface
   *   The called Webpay transaction entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Get the Payment type name.
   *
   * @return \Drupal\Core\StringTranslation\TranslatableMarkup
   *   The payment type name.
   */
  public function getPaymentType();

  /**
   * Get the Quota type name.
   *
   * @return \Drupal\Core\StringTranslation\TranslatableMarkup
   *   The quota type name.
   */
  public function getQuotaType();

}
