<?php

namespace Drupal\Tests\webpay\Functional;

use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Tests\BrowserTestBase;
use Drupal\webpay\Entity\WebpayConfig;

/**
 * Class AddCertificationEnvironmentTest.
 *
 * Test the process to create the certification environment.
 *
 * @package Drupal\tests\webpay\Functional
 *
 * @group webpay
 */
class AddCertificationEnvironmentTest extends BrowserTestBase {

  use StringTranslationTrait;

  /**
   * {@inheritdoc}
   */
  public static $modules = ['webpay', 'block'];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'classy';

  /**
   * {@inheritdoc}
   */
  protected function setUp() {
    parent::setUp();

    $this->drupalPlaceBlock('local_actions_block');
    $this->drupalPlaceBlock('page_title_block');

    $admin_user = $this->drupalCreateUser([
      'webpay administer',
    ]);
    $this->drupalLogin($admin_user);
  }

  /**
   * Test the UI to create the certification environment.
   */
  public function testAdd() {
    $this->drupalGet('admin/config/webpay/webpay_config');
    $this->assertSession()->responseContains($this->t('Add Commerce Certification Configuration'));

    $this->drupalGet('admin/config/webpay/webpay_config/add-certification');
    $this->assertSession()->responseContains($this->t('Are you sure you want to create the certification configuration?'));

    $this->drupalPostForm(NULL, [], $this->t('Confirm'));
    $certification = WebpayConfig::load('certification');
    $this->assertNotNull($certification, 'The certification environment was created.');
  }

}
