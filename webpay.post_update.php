<?php

/**
 * @file
 * Post update functions for Webpay.
 */

use Drupal\Core\Field\BaseFieldDefinition;

/**
 * Clear caches to change the storage handler of Webpay Transaction.
 */
function webpay_post_update_storage_handler_webpay_transaction() {
  // Empty post-update hook.
}

/**
 * Updates the field type of the transaction_date field.
 */
function webpay_post_update_change_transaction_date_field_type() {
  $database = \Drupal::database();
  $entity_definition_update_manager = \Drupal::entityDefinitionUpdateManager();

  $values = $database->select('webpay_transaction')
    ->fields('webpay_transaction', ['id', 'transaction_date'])
    ->execute()
    ->fetchAllKeyed();

  $database->update('webpay_transaction')
    ->fields(['transaction_date' => NULL])
    ->execute();

  $field_storage_definition = $entity_definition_update_manager->getFieldStorageDefinition('transaction_date', 'webpay_transaction');
  $entity_definition_update_manager->uninstallFieldStorageDefinition($field_storage_definition);

  $new_field = BaseFieldDefinition::create('timestamp')
    ->setLabel(t('Transaction date'))
    ->setDisplayConfigurable('view', TRUE);

  $entity_definition_update_manager->installFieldStorageDefinition('transaction_date', 'webpay_transaction', 'webpay_transaction', $new_field);

  foreach ($values as $id => $value) {
    $database->update('webpay_transaction')
      ->fields(['transaction_date' => $value])
      ->condition('id', $id)
      ->execute();
  }

  return t('The transaction_date field was updated.');
}
